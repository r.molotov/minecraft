﻿using UnityEngine;
using System.Collections;

public class UnderWater : MonoBehaviour {

	public Transform water;
	public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "MainCamera") {
			RenderSettings.fog = true;
			RenderSettings.fogColor = new Color (0f, 0.4f, 0.7f, 0.6f);
			RenderSettings.fogDensity = 0.04f;
			water.GetComponent<MeshRenderer> ().enabled = true;
		}
	}
	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "MainCamera") {
			RenderSettings.fog = false;
			water.GetComponent<MeshRenderer>().enabled = false;

		}
	}

	Rigidbody rb;
	public void OnTriggerStay(Collider other)
	{
		if ((rb = other.GetComponent<Rigidbody> ()) != null) {
			// some physics
			float k = Mathf.Clamp01(9.6f - other.transform.position.y);
			rb.AddForce (20 * Vector3.up * k);
			rb.AddForce (-1 * rb.velocity);
		}

	}
}